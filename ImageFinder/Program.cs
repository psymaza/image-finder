﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ImageFinder.Core.Services;

namespace ImageFinder
{
    class Program
    {
        private static Uri _uri;
        private static readonly string DownloadDir;
        private static readonly HtmlLoader HtmlLoader;
        private static readonly HtmlParser HtmlParser;
        private static readonly FileManager FileManager;

        static Program()
        {
            HtmlLoader = new HtmlLoader();
            HtmlParser = new HtmlParser();
            FileManager = new FileManager();
            DownloadDir = Path.Combine(Directory.GetCurrentDirectory(), "download");

            if (!Directory.Exists(DownloadDir))
                Directory.CreateDirectory(DownloadDir);
        }

        static async Task Main(string[] args)
        {
            try
            {
                Console.Write("Url: ");
                _uri = new Uri(Console.ReadLine() ?? throw new InvalidCastException());

                var html = await HtmlLoader.DownloadAsync(_uri);
                var images = HtmlParser.Parse(html);

                images.ToList().ForEach(DownloadFunc);
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Url не может быть пустым");
            }
            catch (UriFormatException)
            {
                Console.WriteLine("Не верный формат url");
            }
            catch (Exception e)
            {
                Console.WriteLine("Не предвиденая ошибка: {0}", GetInnerMessageException(e));
            }
        }

        private static Action<(string src, string name)> DownloadFunc
            => async image =>
                await FileManager.DownloadAsync(new FileInfoDto
                {
                    Uri = image.src.StartsWith('/') ? new Uri(new Uri(_uri.AbsoluteUri), image.src) : new Uri(image.src),
                    FileName = image.name,
                    SavePath = DownloadDir
                });

        private static string GetInnerMessageException(Exception e)
            => e.InnerException is null ? e.Message : GetInnerMessageException(e.InnerException);
    }
}