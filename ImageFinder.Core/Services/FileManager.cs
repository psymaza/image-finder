using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using ImageFinder.Core.Services.Interfaces;

namespace ImageFinder.Core.Services
{
    public class FileInfoDto
    {
        public Uri Uri { get; set; }
        public string SavePath { get; set; }
        public string FileName { get; set; }
    }
    
    public class FileManager : IFileManager
    {
        public async Task<string> DownloadAsync(FileInfoDto fileInfo)
        {
            using var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(fileInfo.Uri);
            return await SaveFileAsync(await response.Content.ReadAsStreamAsync(), fileInfo.SavePath, fileInfo.FileName);
        }

        private async Task<string> SaveFileAsync(Stream stream, string savePath, string fileName)
        {
            var fullFileName = Path.Combine(savePath, string.Join("-", fileName, Guid.NewGuid().ToString()));
            await using var fs = new FileStream(fullFileName, FileMode.Create);
            await stream.CopyToAsync(fs);
            return fullFileName;
        }
    }
}