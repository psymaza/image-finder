using System;
using System.Threading.Tasks;

namespace ImageFinder.Core.Services.Interfaces
{
    public interface ILoader<out TOut, in TIn>
    {
        Task<string> DownloadAsync(TIn args);
    }
}