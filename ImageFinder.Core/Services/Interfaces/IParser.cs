namespace ImageFinder.Core.Services.Interfaces
{
    public interface IParser<out TOut, in TIn>
    {
        TOut Parse(TIn args);
    }
}