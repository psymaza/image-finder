using System.IO;

namespace ImageFinder.Core.Services.Interfaces
{
    public interface IFileManager : ILoader<string, FileInfoDto>
    {
    }
}