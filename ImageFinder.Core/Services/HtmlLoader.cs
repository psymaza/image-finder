using System;
using System.Net.Http;
using System.Threading.Tasks;
using ImageFinder.Core.Services.Interfaces;

namespace ImageFinder.Core.Services
{
    public class HtmlLoader : ILoader<string, Uri>
    {
        public async Task<string> DownloadAsync(Uri uri)
        {
            using var httpClient = new HttpClient();
            return await httpClient.GetStringAsync(uri);
        }
    }
}