using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ImageFinder.Core.Services.Interfaces;

namespace ImageFinder.Core.Services
{
    public class HtmlParser : IParser<IEnumerable<(string src, string name)>, string>
    {
        private const string SrcGrpName = "src";
        private Regex _imgSrcRegex = new(@$"<\s?img.*(src=[""'`](?<{SrcGrpName}>.*?)[""`])");
        private Regex _nameFromSrcRegex = new(@"([\w\d\-_]+\.jpg|png)");

        public IEnumerable<(string src, string name)> Parse(string html)
            => _imgSrcRegex.Matches(html)
                .Select(e => e.Groups[SrcGrpName].Value)
                .Select(e => (e, _nameFromSrcRegex.Match(e).Value))
                .ToList();
    }
}